package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Professor;

@Stateless
public class ProfessorDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Professor c) { em.persist(c); }
	public void atualizar(Professor c) { em.merge(c); }
	public void remover(Professor c) { c = em.find(Professor.class, c.getId()); em.remove(c); }
	
	@SuppressWarnings("unchecked")
	public List<Professor> listar() {
		String qs = "select c from Professor c";
		Query q = em.createQuery(qs);
		return (List<Professor>) q.getResultList();
	}
	
	public Professor buscar(String nome) {
		String qs = "select c from Professor c where c.nome = :nome";
		Query q = em.createQuery(qs);
		q.setParameter("nome", nome);
		try {
			return (Professor) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Professor buscarProfessorID(long id) {
		return em.find(Professor.class, id);
	}
}
