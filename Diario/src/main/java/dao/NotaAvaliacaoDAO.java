package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import dominio.NotaAvaliacao;

@Stateless
public class NotaAvaliacaoDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(NotaAvaliacao c) { em.persist(c); }
	public void atualizar(NotaAvaliacao c) { em.merge(c); }
	public void remover(NotaAvaliacao c) { c = em.find(NotaAvaliacao.class, c.getId()); em.remove(c); }
	
	@SuppressWarnings("unchecked")
	public List<NotaAvaliacao> listar() {
		String qs = "select c from NotaAvaliacao c";
		Query q = em.createQuery(qs);
		return (List<NotaAvaliacao>) q.getResultList();
	}
	public  NotaAvaliacao buscar(long id) {
		String qs = "select c.nota, f.nome from NotaAvaliacao, Avaliacao  f where c.avaliacao_id = f.avaliacao_id";
		Query q = em.createQuery(qs);
		q.setParameter("id", id);
		try {
			return (NotaAvaliacao) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public NotaAvaliacao buscarNotaAvaliacaoID(long id) {
		return em.find(NotaAvaliacao.class, id);
	}
}
