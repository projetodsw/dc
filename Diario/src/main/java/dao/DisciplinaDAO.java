package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import dominio.Disciplina;

@Stateless
public class DisciplinaDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Disciplina c) { em.persist(c); }
	public void atualizar(Disciplina c) { em.merge(c); }
	public void remover(Disciplina c) { c = em.find(Disciplina.class, c.getId()); em.remove(c); }
	@SuppressWarnings("unchecked")
	public List<Disciplina> listar() {
		String qs = "select c from Disciplina c";
		Query q = em.createQuery(qs);
		return (List<Disciplina>) q.getResultList();
	}
	
	public Disciplina buscar(String nome) {
		String qs = "select c.nome from Disciplina c";
		Query q = em.createQuery(qs);
		q.setParameter("nome", nome);
		try {
			return (Disciplina) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	public Disciplina buscarDisciplinaID(long id) {
		return em.find(Disciplina.class, id);
	}
}
