package dao;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Aula;

@Stateless
public class AulaDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Aula c) { em.persist(c); }
	public void atualizar(Aula c) { em.merge(c); }
	public void remover(Aula c) { c = em.find(Aula.class, c.getId()); em.remove(c); }
	
	@SuppressWarnings("unchecked")
	public List<Aula> listar() {
		String qs = "select c from Aula c";
		Query q = em.createQuery(qs);
		return (List<Aula>) q.getResultList();
	}
	public Aula buscar(String titulo) {
		String qs = "select a Aula a where a.titulo = :titulo";
		Query q = em.createQuery(qs);
		q.setParameter("titulo", titulo);
		try {
			return (Aula) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Aula buscarAulaID(long id) {
		return em.find(Aula.class, id);
	}
}
