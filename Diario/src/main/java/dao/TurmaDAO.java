package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Turma;

@Stateless
public class TurmaDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Turma c) { em.persist(c); }
	public void atualizar(Turma c) { em.merge(c); }
	public void remover(Turma c) { c = em.find(Turma.class, c.getId()); em.remove(c); }
	
	@SuppressWarnings("unchecked")
	public List<Turma> listar() {
		String qs = "select c from Turma c";
		Query q = em.createQuery(qs);
		return (List<Turma>) q.getResultList();
	}
	
	public Turma buscar(String nome) {
		String qs = "select c from Turma c where c.nome = :nome";
		Query q = em.createQuery(qs);
		q.setParameter("nome", nome);
		try {
			return (Turma) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	public Turma buscarPorNivelEscolar(int i) {
		String qs = "select c.nivelEscolar from turma";
		Query q = em.createQuery(qs);
		q.setParameter("nivelEscolar", i);
		try {
			return (Turma) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	public Turma buscarTurmaID(long id) {
		return em.find(Turma.class, id);
	}
}
