package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Matricula;

@Stateless
public class MatriculaDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Matricula c) { em.persist(c); }
	public void atualizar(Matricula c) { em.merge(c); }
	public void remover(Matricula c) { c = em.find(Matricula.class, c.getId()); em.remove(c); }
	
	@SuppressWarnings("unchecked")
	public List<Matricula> listar() {
		String qs = "select c from Matricula c";
		Query q = em.createQuery(qs);
		return (List<Matricula>) q.getResultList();
	}
	
	public Matricula buscar(long matricula) {
		String qs = "select c from Matricula c where c.matricula = :matricula";
		Query q = em.createQuery(qs);
		q.setParameter("matricula", matricula);
		try {
			return (Matricula) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Matricula buscarMatriculaID(long id) {
		return em.find(Matricula.class, id);
	}
}
