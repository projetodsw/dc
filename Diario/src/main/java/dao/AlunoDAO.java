package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Aluno;

@Stateless
public class AlunoDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Aluno c) { em.persist(c); }
	public void atualizar(Aluno c) { em.merge(c); }
	public void remover(Aluno c) { c = em.find(Aluno.class, c.getId()); em.remove(c); }
	
	@SuppressWarnings("unchecked")
	public List<Aluno> listar() {
		String qs = "select c.id,c.nome from Aluno c";
		Query q = em.createQuery(qs);
		return (List<Aluno>) q.getResultList();
	}
	
	public Aluno buscar(String nome) {
		String qs = "select c from Aluno c where c.nome = :nome";
		Query q = em.createQuery(qs);
		q.setParameter("nome", nome);
		try {
			return (Aluno) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	public Aluno buscarAlunoID(long id) {
		return em.find(Aluno.class, id);
	}
}
