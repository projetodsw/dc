package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Frequencia;

@Stateless
public class FrequenciaDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Frequencia c) { em.persist(c); }
	public void atualizar(Frequencia c) { em.merge(c); }
	public void remover(Frequencia c) { c = em.find(Frequencia.class, c.getId()); em.remove(c); }

	@SuppressWarnings("unchecked")
	public List<Frequencia> listar() {
		String qs = "select c from Frequencia c";
		Query q = em.createQuery(qs);
		return (List<Frequencia>) q.getResultList();
	}
	public Frequencia buscar(long id){
		String qs = "select c.id, f.datainicio from Frequencia c, aula f where c.aula_id = f.aula_id";
		Query q = em.createQuery(qs);
		q.setParameter("id", id);
		try {
			return (Frequencia) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Frequencia buscarFrequenciaID(long id) {
		return em.find(Frequencia.class, id);
	}
}
