package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dominio.Avaliacao;

@Stateless
public class AvaliacaoDAO {
	@PersistenceContext
	private EntityManager em;
	
	public void salvar(Avaliacao c) { em.persist(c); }
	public void atualizar(Avaliacao c) { em.merge(c); }
	public void remover(Avaliacao c) { c = em.find(Avaliacao.class, c.getId()); em.remove(c); }
	
	@SuppressWarnings("unchecked")
	public List<Avaliacao> listar() {
		String qs = "select c from Avaliacao c";
		Query q = em.createQuery(qs);
		return (List<Avaliacao>) q.getResultList();
	}
	
	public Avaliacao buscar(String titulo) {
		String qs = "select c.titulo from Avaliacao c";
		Query q = em.createQuery(qs);
		q.setParameter("titulo", titulo);
		try {
			return (Avaliacao) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Avaliacao buscarAvaliacaoID(long id) {
		return em.find(Avaliacao.class, id);
	}
}
