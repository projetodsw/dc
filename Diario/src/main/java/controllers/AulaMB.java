package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import dominio.Aula;
import negocio.AulaService;

@ManagedBean
@RequestScoped
public class AulaMB {
	private Aula aula;
	@EJB
	private AulaService aulaService;
	private List<Aula> listaAulas;
	
	
	
	
	public Aula getAula() {
		return aula;
	}

	public void setAula(Aula aula) {
		this.aula = aula;
	}

	public List<Aula> getListaAulas() {
		return listaAulas;
	}

	public void setListaAulas(List<Aula> listaAulas) {
		this.listaAulas = listaAulas;
	}

	public AulaMB() {
		aulaService.cadastrarAulaS(aula);
		aula = new Aula();
		listaAulas = new ArrayList<Aula>();
	}
	
	public String cadastrar() {
		aula = new Aula();
		return "/interna/admin/listaAlunos.jsf";
	}
	public String novo() {
		return "";
	}

	public String apagar(Aula aulaARemover) {
		listaAulas.remove(aulaARemover);
		FacesMessage msg = new FacesMessage("Aluno removido com sucesso");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage("", msg);
		aula = new Aula();
		return "/interna/admin/listaAlunos.jsf";
	}

}
