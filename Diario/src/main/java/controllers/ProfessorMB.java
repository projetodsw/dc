package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import dominio.Professor;
import negocio.ProfessorService;

@ManagedBean
@RequestScoped
public class ProfessorMB {

	private Professor professor;
	
	private List<Professor> listaProfessores;
	@EJB
	private ProfessorService professorService;

	public ProfessorMB() {
		professor = new Professor();
		listaProfessores = new ArrayList<Professor>();
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public List<Professor> getListaProfessores() {
		setListaProfessores(professorService.listarProfessor());
		return listaProfessores;
	}

	public void setListaProfessores(List<Professor> listaProfessores) {
		this.listaProfessores = listaProfessores;
	}

	public String cadastrar() {
		professorService.cadastrarProfessor(professor);
		professor = new Professor();
		return "/interna/admin/listaProfessores.jsf";
	}

	public String novo() {
		return "/interna/admin/cadastraProfessor.jsf";
	}
	
	public String apagar(Professor profARemover) {
		listaProfessores.remove(profARemover);
		professorService.removerProfessor(profARemover);
		FacesMessage msg = new FacesMessage("Professor removido com sucesso");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage("", msg);
		professor = new Professor();
		return "/interna/admin/listaProfessores.jsf";
	}

}
