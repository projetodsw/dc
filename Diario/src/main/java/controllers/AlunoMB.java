package controllers;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import dominio.Aluno;
import negocio.AlunoService;


@ManagedBean
@RequestScoped
public class AlunoMB {
	private Aluno aluno;
	private List<Aluno> listaAlunos;
	@EJB
	private AlunoService alunoService;

	public AlunoMB() {
		aluno = new Aluno();
		listaAlunos = new ArrayList<Aluno>();
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public List<Aluno> getListaAlunos() {
		setListaAlunos(alunoService.listarAluno());
		return listaAlunos;
	}

	public void setListaAlunos(List<Aluno> listaAlunos) {
		this.listaAlunos = listaAlunos;
	}

	public String cadastrar() {
		alunoService.cadastrarAluno(aluno);
		aluno = new Aluno();
		return "/interna/admin/listaAlunos.jsf";
	}

	public String novo() {
		return "/interna/admin/cadastraAluno.jsf";
	}

}
