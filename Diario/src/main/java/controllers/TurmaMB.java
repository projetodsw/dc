package controllers;

import java.util.ArrayList;
import java.util.List;


import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import dominio.Turma;
import negocio.TurmaService;

@ManagedBean
@RequestScoped
public class TurmaMB {
	private Turma turma;
	private List<Turma> listaTurmas;
	@EJB
	private TurmaService turmaService;
	
	public Turma getTurma() {
		return turma;
	}
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	public List<Turma> getListaturmaes() {
		setListaturmaes(turmaService.listarTurma());
		return listaTurmas;
	}
	public void setListaturmaes(List<Turma> listaTurmas) {
		this.listaTurmas = listaTurmas;
	}
	public TurmaMB(){
		turma=new Turma();
		listaTurmas = new ArrayList<Turma>();
	}
	
	public String cadastrar() {
		turmaService.cadastrarTurma(turma);		
		turma = new Turma();
		return "/interna/admin/listaTurmaes.jsf";
	}

	public String novo() {
		return "/interna/admin/cadastraTurma.jsf";
	}
	
	public String apagar(Turma profARemover) {
		listaTurmas.remove(profARemover);
		FacesMessage msg = new FacesMessage("Turma removido com sucesso");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage("", msg);
		turma = new Turma();
		return "/interna/admin/listaTurmaes.jsf";
	}

	
}
