package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import dominio.NotaAvaliacao;
import negocio.NotaAvaliacaoService;

@ManagedBean
@RequestScoped
public class NotaAvaliacaoMB {
	private NotaAvaliacao notaAvaliacao;
	
	@EJB
	private NotaAvaliacaoService notaAvaliacaoService;
	
	private List<NotaAvaliacao> listaNotaAvaliacaes;

	public NotaAvaliacao getNotaAvaliacao() {
		return notaAvaliacao;
	}

	public void setNotaAvaliacao(NotaAvaliacao notaAvaliacao) {
		this.notaAvaliacao = notaAvaliacao;
	}

	public List<NotaAvaliacao> getListadeNotaAvaliacaes() {
		setListadeNotaAvaliacaes(notaAvaliacaoService.listarNotaAvaliacao());
		return  listaNotaAvaliacaes;
	}

	public void setListadeNotaAvaliacaes(List<NotaAvaliacao> listaNotaAvaliacaes) {
		this.listaNotaAvaliacaes = listaNotaAvaliacaes;
	}
	
	public NotaAvaliacaoMB(){
		listaNotaAvaliacaes=new ArrayList<NotaAvaliacao>();
	}
	
	public void cadastrar() {
		notaAvaliacaoService.cadastrarNotaAvaliacao(notaAvaliacao);
		notaAvaliacao = new NotaAvaliacao();
		
	}

}
