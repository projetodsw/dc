package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import dao.DisciplinaDAO;
import dominio.Disciplina;

@ManagedBean
@RequestScoped
public class DisciplinaMB {
	private Disciplina disciplina;
	@Inject
	private DisciplinaDAO disciplinaDAO;
	
	private List<Disciplina> listaDisciplinas;

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public DisciplinaDAO getDisciplinaDAO() {
		return disciplinaDAO;
	}

	public void setDisciplinaDAO(DisciplinaDAO disciplinaDAO) {
		this.disciplinaDAO = disciplinaDAO;
	}

	public List<Disciplina> getListaDisciplinas() {
		setListaDisciplinas(disciplinaDAO.listar());
		return listaDisciplinas;
	}

	public void setListaDisciplinas(List<Disciplina> listaDisciplinas) {
		this.listaDisciplinas = listaDisciplinas;
	}
	
	public DisciplinaMB(){
		disciplina=new Disciplina();
		listaDisciplinas = new ArrayList<Disciplina>();
	}
	public String cadastrar() {
		Disciplina c = disciplinaDAO.buscar(disciplina.getNome());
		if (c == null) {
			disciplinaDAO.salvar(disciplina);
		} else {
			disciplinaDAO.atualizar(disciplina);
		}
		FacesMessage msg = new FacesMessage("Avaliacao adicionado com sucesso");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage("", msg);
		disciplina = new Disciplina();
		return "/interna/prof/listaAvaliacaos.jsf";
	}
	public String novo() {
		return "/interna/prof/cadastraAvaliacao.jsf";
	}

	
	public String apagar(Disciplina disciplinaRemover) {
		Disciplina c = disciplinaDAO.buscar(disciplinaRemover.getNome());
		disciplinaDAO.remover(c);
		
		FacesMessage msg = new FacesMessage("Avaliacao removido com sucesso");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage("", msg);
		disciplina = new Disciplina();
		return "/interna/prof/listaAvaliacaos.jsf";
	}
	
}
