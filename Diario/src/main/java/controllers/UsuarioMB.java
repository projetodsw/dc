package controllers;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import dominio.Usuario;
import negocio.LoginService;

@ManagedBean
@RequestScoped
public class UsuarioMB {
	
	private Usuario usuario;
	
	@EJB
	private LoginService loginService;
	

	public UsuarioMB() {
		usuario = new Usuario();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}	


	public String login() {
		int res = loginService.login(usuario.getLogin(), usuario.getSenha());
		switch(res){
		
		//Admin
		case 0: return "/interna/admin/home.jsf";
		//Professor
		case 1: return "/interna/prof/home.jsf";
		//Aluno
		case 2: return "";
		case -1: FacesMessage msg1 = new FacesMessage("Usuario ou senha icorretos");
		msg1.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage("", msg1);
		return null;
		
		default: FacesMessage msg2 = new FacesMessage("Usuario n�o existe");
		msg2.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage("", msg2);
		return null;
		
		}
	}
	
	public String irParaHomeAdmin() {
		return "/interna/admin/home.jsf";
	}
	
	public String irParaListaProfessores() {
		return "/interna/admin/listaProfessores.jsf";
	}
	
	public String irParaCadastrarProfessores() {
		return "/interna/admin/cadastraProfessores.jsf";
	}
	
	public String irParaListaAlunos() {
		return "/interna/admin/listaAlunos.jsf";
	}
	
	public String irParaCadastrarAlunos() {
		return "/interna/admin/cadastraAluno.jsf";
	}
}
