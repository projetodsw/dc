package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import dao.AvaliacaoDAO;
import dominio.Avaliacao;

@ManagedBean
@RequestScoped
public class AvaliacaoMB {
	private Avaliacao avaliacao;
	@Inject
	private AvaliacaoDAO avaliacaoDAO;
	
	private List<Avaliacao> listaAvaliacoes;
	
	
	public Avaliacao getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(Avaliacao avaliacao) {
		this.avaliacao = avaliacao;
	}
	public AvaliacaoDAO getAvaliacaoDAO() {
		return avaliacaoDAO;
	}
	public void setAvaliacaoDAO(AvaliacaoDAO avaliacaoDAO) {
		this.avaliacaoDAO = avaliacaoDAO;
	}
	public List<Avaliacao> getListaAvaliacoes() {
		setListaAvaliacoes(avaliacaoDAO.listar());
		return listaAvaliacoes;
	}
	public void setListaAvaliacoes(List<Avaliacao> listaAvaliacaos) {
		this.listaAvaliacoes = listaAvaliacaos;
	}
	
	public AvaliacaoMB() {
		avaliacao = new Avaliacao();
		listaAvaliacoes = new ArrayList<Avaliacao>();
	}
	
	public String cadastrar() {
		Avaliacao c = avaliacaoDAO.buscar(avaliacao.getTitulo());
		if (c == null) {
			avaliacaoDAO.salvar(avaliacao);
		} else {
			avaliacaoDAO.atualizar(avaliacao);
		}
		FacesMessage msg = new FacesMessage("Avaliacao adicionado com sucesso");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage("", msg);
		avaliacao = new Avaliacao();
		return "/interna/prof/listaAvaliacaos.jsf";
	}
	public String novo() {
		return "/interna/prof/cadastraAvaliacao.jsf";
	}

	
	public String apagar(Avaliacao avaliacaoARemover) {
		Avaliacao c = avaliacaoDAO.buscar(avaliacaoARemover.getTitulo());
		avaliacaoDAO.remover(c);
		
		FacesMessage msg = new FacesMessage("Avaliacao removido com sucesso");
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage("", msg);
		avaliacao = new Avaliacao();
		return "/interna/prof/listaAvaliacaos.jsf";
	}
}
