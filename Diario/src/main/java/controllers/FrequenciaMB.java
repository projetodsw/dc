package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import dominio.Frequencia;
import negocio.FrequenciaService;

@ManagedBean
@RequestScoped
public class FrequenciaMB {
	private Frequencia frequencia;
	@EJB
	private FrequenciaService frequenciaService;
	
	private List<Frequencia> listaFrequencias;
	
	
	public Frequencia getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}
	public List<Frequencia> getListaFrequencias() {
		setListaFrequencias(frequenciaService.listarFrequencia());
		return listaFrequencias;
	}
	public void setListaFrequencias(List<Frequencia> listaFrequencias) {
		this.listaFrequencias = listaFrequencias;
	}
	
	public FrequenciaMB() {
		frequencia = new Frequencia();
		listaFrequencias = new ArrayList<Frequencia>();
	}
	public void cadastrar() {
		frequenciaService.cadastrarFrequencia(frequencia);
		frequencia = new Frequencia();
	}	
}
