package dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="notaavaliacao")
public class NotaAvaliacao {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	@Column(name= "nota", nullable=false)
	private double nota;
	
	@ManyToOne 
	private Avaliacao avaliacao; 

	@ManyToOne 
	private Aluno aluno; 
	
	public NotaAvaliacao(){
		this.aluno = new Aluno();
		this.avaliacao=new Avaliacao();
	}
	public NotaAvaliacao(int nota){
		this.nota=nota;
		this.aluno = new Aluno();
		this.avaliacao=new Avaliacao();
	}
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getNota() {
		return nota;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}
	public Avaliacao getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(Avaliacao avaliacao) {
		this.avaliacao = avaliacao;
	}
	public Aluno getAluno() {
		return aluno;
	}
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	
	@Override
	public String toString() {
		return this.id + " - " + this.nota;
	}
	
}
