package dominio;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="aluno")
public class Aluno {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "aluno_id", unique = true, nullable = false)
	private long id;
	
	@Column(name= "nome", nullable=false) 
	private String nome;

	@ManyToMany(mappedBy="aluno", cascade= CascadeType.ALL)
	private List<Usuario> usuario;
	
	@OneToMany(mappedBy="aluno", cascade= CascadeType.ALL) 
	private List<NotaAvaliacao> notaAvaliacao;
	
	@OneToMany(mappedBy="aluno", cascade= CascadeType.ALL)
	private List<Matricula> matricula;
	
	
	
	public Aluno(){
		this.matricula = new ArrayList<Matricula>();
		this.notaAvaliacao = new ArrayList<NotaAvaliacao>();
		this.usuario = new ArrayList<Usuario>();
	}
	
	public Aluno(String nome) {
		this.nome = nome;
		this.matricula = new ArrayList<Matricula>();
		this.notaAvaliacao = new ArrayList<NotaAvaliacao>();
		this.usuario = new ArrayList<Usuario>();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Usuario> getUsuario() {
		return usuario;
	}

	public void setUsuario(List<Usuario> usuario) {
		this.usuario = usuario;
	}

	public List<NotaAvaliacao> getNotaAvaliacao() {
		return notaAvaliacao;
	}

	public void setNotaAvaliacao(List<NotaAvaliacao> notaAvaliacao) {
		this.notaAvaliacao = notaAvaliacao;
	}

	public List<Matricula> getMatricula() {
		return matricula;
	}

	public void setMatricula(List<Matricula> matricula) {
		this.matricula = matricula;
	}

	@Override
	public String toString() {
		return "Aluno [id=" + id + ", nome=" + nome + ", usuario=" + usuario + ", notaAvaliacao=" + notaAvaliacao
				+ ", matricula=" + matricula + "]";
	}
}
