package dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity 
@Table(name="aula")
public class Aula {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	
	@Column(name= "titulo", nullable=false)
	private String titulo;
	@Column(name= "dataInicio", nullable=false)
	private Date dataInicio;
	@Column(name= "dataFim", nullable=true)
	private Date dataFim;
	@Column(name= "anotacao", nullable=true)
	private String anotacao;
	@Column(name= "arquivo", nullable=true)
	private String arquivo;
	
	@ManyToOne
	private Turma turma;
	@ManyToOne
	private Professor professor;
	@ManyToOne
	private Disciplina disciplina;
	@OneToMany(mappedBy="aula", cascade= CascadeType.ALL)
	private Collection<Frequencia> frequencia;
	
	
	public Aula(String titulo,Date inicio,Disciplina disciplina, Professor professor, Turma turma){
		this.titulo = titulo;
		this.dataFim = new Date(Calendar.DAY_OF_YEAR);
		this.dataInicio = inicio;
		this.anotacao = "";
		this.frequencia = new ArrayList<Frequencia>();
		this.disciplina = disciplina;
		this.professor = professor;
		this.turma = turma;
		this.arquivo = "";
	}
	public Aula(){
		this.frequencia = new ArrayList<Frequencia>();
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public String getAnotacao() {
		return anotacao;
	}
	public void setAnotacao(String anotacao) {
		this.anotacao = anotacao;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Turma getTurma() {
		return turma;
	}
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	public Professor getProfessor() {
		return professor;
	}
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	public Disciplina getDisciplina() {
		return disciplina;
	}
	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
	public String getArquivo() {
		return arquivo;
	}
	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}
	
	public Collection<Frequencia> getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(Collection<Frequencia> frequencia) {
		this.frequencia = frequencia;
	}
	@Override
	public String toString() {
		return "Aula [id=" + id + ", titulo=" + titulo + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim
				+ ", anotacao=" + anotacao + ", arquivo=" + arquivo + ", turma=" + turma + ", professor=" + professor
				+ ", disciplina=" + disciplina + ", frequencia=" + frequencia + "]";
	}	
}
