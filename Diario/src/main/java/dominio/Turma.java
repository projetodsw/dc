package dominio;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="turma")
public class Turma {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
	@Column(name= "nome", nullable=false)
	private String nome;
	@Column(name= "capacidade", nullable=false)
	private int capacidade;
	@Column(name= "nivelEscolar", nullable=false)
	private int nivelEscolar;
	@Column(name= "ano", nullable=false)
	private int ano;
	@Column(name= "situacao", nullable=false)
	private boolean situacao; //esta ativa=true ou inativa=false
	
	@OneToMany(mappedBy="turma", cascade= CascadeType.ALL) 
	private Collection<Aula> aula;
	@OneToMany(mappedBy="turma", cascade= CascadeType.ALL) 
	private Collection<Matricula> matricula;
	@OneToMany(mappedBy="turma", cascade= CascadeType.ALL) 
	private Collection<Avaliacao> avaliacao;
	
	
	public Turma(){
		this.aula = new ArrayList<Aula>();
		this.matricula = new ArrayList<Matricula>();
	}
	public Turma(String nome, int capacidade, int nivelEscolar, int ano){
		this.nome = nome;
		this.capacidade = capacidade;
		this.nivelEscolar = nivelEscolar;
		this.ano = ano;
		this.aula = new ArrayList<Aula>();
		this.matricula = new ArrayList<Matricula>();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}
	public int getNivelEscolar() {
		return nivelEscolar;
	}
	public void setNivelEscolar(int nivelEscolar) {
		this.nivelEscolar = nivelEscolar;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public boolean isSituacao() {
		return situacao;
	}
	public void setSituacao(boolean situacao) {
		this.situacao = situacao;
	}
	
	public Collection<Aula> getAula() {
		return aula;
	}
	public void setAula(Collection<Aula> aula) {
		this.aula = aula;
	}
	public Collection<Matricula> getMatricula() {
		return matricula;
	}
	public void setMatricula(Collection<Matricula> matricula) {
		this.matricula = matricula;
	}
	public Collection<Avaliacao> getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(Collection<Avaliacao> avaliacao) {
		this.avaliacao = avaliacao;
	}
	@Override
	public String toString() {
		return "Turma [id=" + id + ", nome=" + nome + ", capacidade=" + capacidade + ", nivelEscolar=" + nivelEscolar
				+ ", ano=" + ano + ", situacao=" + situacao + ", aula=" + aula + ", matricula=" + matricula
				+ ", avaliacao=" + avaliacao + "]";
	}
}
