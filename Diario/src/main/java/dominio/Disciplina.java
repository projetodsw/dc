package dominio;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="disciplina")
public class Disciplina {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
	@Column(name= "nome", nullable=false)
	private String nome;

	@OneToMany(mappedBy = "disciplina", cascade= CascadeType.ALL)
	private Collection<Aula> aula;
	@OneToMany(mappedBy = "disciplina", cascade= CascadeType.ALL)
	private Collection<Avaliacao> avaliacao;
	
	public Disciplina(){
		this.aula = new ArrayList<Aula>();
		this.avaliacao = new ArrayList<Avaliacao>();
	}
	public Disciplina(String nome){
		this.nome=nome;
		this.aula = new ArrayList<Aula>();
		this.avaliacao = new ArrayList<Avaliacao>();
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public Collection<Aula> getAula() {
		return aula;
	}
	public Collection<Avaliacao> getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(Collection<Avaliacao> avaliacao) {
		this.avaliacao = avaliacao;
	}
	public void setAula(Collection<Aula> aula) {
		this.aula = aula;
	}
	@Override
	public String toString() {
		return "Disciplina [id=" + id + ", nome=" + nome + ", aula=" + aula + ", avaliacao=" + avaliacao + "]";
	}
}
