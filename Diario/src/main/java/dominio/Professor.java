package dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="professor")
public class Professor {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
	@Column(name = "nome", nullable = false)
	private String nome;
	@Column(name= "email", nullable=true)
	private String email;
	@Column(name= "telefone", nullable=true)
	private String telefone;
	@Column(name= "dataNascimento", nullable=true)
	private Date dataNascimento;
	
	@OneToMany(mappedBy="professor", cascade= CascadeType.ALL)
	private Collection<Aula> aula;
	@ManyToMany(mappedBy="professor", cascade= CascadeType.ALL)
	private Collection<Usuario> usuario;
	
	public Professor(String nome, String email) {
		this.nome = nome;
		this.email=email;
		this.telefone = "";
		this.dataNascimento = new Date(Calendar.DAY_OF_YEAR);
		this.usuario = new ArrayList<Usuario>();
		this.aula=  new ArrayList<Aula>();
	}
	public Professor() {
		this.usuario = new ArrayList<Usuario>();
		this.aula=  new ArrayList<Aula>();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public Collection<Aula> getAula() {
		return aula;
	}
	public void setAula(Collection<Aula> aula) {
		this.aula = aula;
	}
	public Collection<Usuario> getUsuario() {
		return usuario;
	}
	public void setUsuario(Collection<Usuario> usuario) {
		this.usuario = usuario;
	}
	@Override
	public String toString() {
		return "Professor [id=" + id + ", nome=" + nome + ", email=" + email + ", telefone=" + telefone
				+ ", dataNascimento=" + dataNascimento + ", aula=" + aula + ", usuario=" + usuario + "]";
	}
	

}