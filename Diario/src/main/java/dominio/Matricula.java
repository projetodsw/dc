package dominio;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="matricula")
public class Matricula {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
	@Column(name= "matricula", nullable=false)
	private long matricula;
	
	@ManyToOne
	private Aluno aluno;
	@ManyToOne
	private Turma turma;
	@OneToMany(mappedBy="matricula", cascade= CascadeType.ALL)
	private Collection<Frequencia> frequencia;
	
	public Matricula(){
		frequencia = new ArrayList<Frequencia>();
	}
	public Matricula(long matricula, Aluno aluno, Turma turma){
		this.matricula=matricula;
		this.aluno=aluno;
		this.turma=turma;
		frequencia = new ArrayList<Frequencia>();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getMatricula() {
		return matricula;
	}
	public void setMatricula(long matricula) {
		this.matricula = matricula;
	}
	public Aluno getAluno() {
		return aluno;
	}
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	public Turma getTurma() {
		return turma;
	}
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	
	public Collection<Frequencia> getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(Collection<Frequencia> frequencia) {
		this.frequencia = frequencia;
	}
	@Override
	public String toString() {
		return "Matricula [id=" + id + ", matricula=" + matricula + ", aluno=" + aluno + ", turma=" + turma
				+ ", frequencia=" + frequencia + "]";
	}
}
