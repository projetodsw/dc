package dominio;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "usuario")
public class Usuario {
	@Id
	@Column(unique=true,nullable=false)
	private String login;
	@Column(name = "senha", nullable = false)
	private String senha;
	
	//permissoes: 0 - Admin; 1 - Professor; 2 - Aluno;
	@Column(name = "permissao", nullable = false)@Max(value=2)@Min(value=0)
	private int permissao;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "usuario_professor", joinColumns = {
			@JoinColumn(name = "usuario_id") }, inverseJoinColumns = {
					@JoinColumn(name = "professor_id") })
	private Collection<Professor> professor;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "usuario_aluno", joinColumns = {
			@JoinColumn(name = "usuario_id") }, inverseJoinColumns = {
					@JoinColumn(name = "aluno_id") })
	private Collection<Aluno> aluno;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getPermissao() {
		return permissao;
	}

	public void setPermissao(int tipo) {
		this.permissao = tipo;
	}

	public Usuario() {
		this.professor = new ArrayList<Professor>();
		this.aluno = new ArrayList<Aluno>();
	}

	public Usuario(String login, String senha, int tipo) {
		this.login = login;
		this.senha = senha;
		this.permissao = tipo;
		this.professor = new ArrayList<Professor>();
		this.aluno = new ArrayList<Aluno>();
		
	}
	
	public Collection<Professor> getProfessor() {
		return professor;
	}

	public void setProfessor(Collection<Professor> professor) {
		this.professor = professor;
	}

	public Collection<Aluno> getAluno() {
		return aluno;
	}

	public void setAluno(Collection<Aluno> aluno) {
		this.aluno = aluno;
	}

	@Override
	public String toString() {
		return "Usuario [login=" + login + ", senha=" + senha + ", permissao=" + permissao + ", professor=" + professor
				+ ", aluno=" + aluno + "]";
	}
}
