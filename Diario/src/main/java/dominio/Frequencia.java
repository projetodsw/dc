package dominio;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="frequencia")
public class Frequencia {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	
	@Column(name= "falta", nullable=false)
	private int falta;
	@Column(name= "data", nullable=false)
	private Date data;
	
	@ManyToOne
	private Matricula matricula;
	
	@ManyToOne
	private Aula aula;
	
	public Frequencia(){
	}
	
	public Frequencia(int falta){
		this.falta=falta;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getFalta() {
		return falta;
	}
	public void setFalta(int falta) {
		this.falta = falta;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Aula getAula() {
		return aula;
	}

	public void setAula(Aula aula) {
		this.aula = aula;
	}

	public Matricula getMatricula() {
		return matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}	
	
	@Override
	public String toString() {
		return this.id + " - " + this.falta +" - " + this.data;
	}
}
