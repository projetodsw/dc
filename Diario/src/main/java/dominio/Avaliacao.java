package dominio;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="avaliacao")
public class Avaliacao {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(name = "id", unique = true, nullable = false)
	private long id;
	@Column(name= "titulo", nullable=false)
	private String titulo;
	
	@Column(name= "descricao", nullable=false)
	private String descricao;
	@Column(name= "peso", nullable=false)
	private double peso;
	@Column(name= "notaMaxima", nullable=false)
	private double notaMaxima;
	//existem n unidades em um bimestre 
	@Column(name= "unidade", nullable=false)
	private int unidade;
	//existem 4 bimestres no ano, ent�o o maximo � 4 e o minimo � 1
	@Column(name= "bimestre", nullable=false)
	private int bimestre;
	
	@OneToMany(mappedBy="avaliacao", cascade= CascadeType.ALL) 
	private Collection<NotaAvaliacao> notaAvaliacao;
	@ManyToOne
	private Disciplina disciplina;
	@ManyToOne
	private Turma turma;
	
	public Avaliacao(){
	this.notaAvaliacao= new ArrayList<NotaAvaliacao>();
		
	}
	public Avaliacao(String titulo, int unidade, int bimestre, Turma turma, Disciplina disciplina){
		this.titulo=titulo;
		this.descricao = "";
		this.peso=1;
		this.notaMaxima = 100;
		this.notaAvaliacao= new ArrayList<NotaAvaliacao>();
		this.disciplina=disciplina;
		this.turma=turma;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String gettitulo() {
		return descricao;
	}
	public void setTitulo(String descricao) {
		this.descricao = descricao;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getTitulo() {
		return titulo;
	}
	public double getNotaMaxima() {
		return notaMaxima;
	}
	public void setNotaMaxima(double notaMaxima) {
		this.notaMaxima = notaMaxima;
	}
	public int getUnidade() {
		return unidade;
	}
	public void setUnidade(int unidade) {
		this.unidade = unidade;
	}
	public int getBimestre() {
		return bimestre;
	}
	public void setBimestre(int bimestre) {
		this.bimestre = bimestre;
	}
	public Disciplina getDisciplina() {
		return disciplina;
	}
	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
	public Turma getTurma() {
		return turma;
	}
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	
	public Collection<NotaAvaliacao> getNotaAvaliacao() {
		return notaAvaliacao;
	}
	public void setNotaAvaliacao(Collection<NotaAvaliacao> notaAvaliacao) {
		this.notaAvaliacao = notaAvaliacao;
	}
	@Override
	public String toString() {
		return "Avaliacao [id=" + id + ", titulo=" + titulo + ", descricao=" + descricao + ", peso=" + peso
				+ ", notaMaxima=" + notaMaxima + ", unidade=" + unidade + ", bimestre=" + bimestre + ", notaAvaliacao="
				+ notaAvaliacao + ", disciplina=" + disciplina + ", turma=" + turma + "]";
	}
	
}
