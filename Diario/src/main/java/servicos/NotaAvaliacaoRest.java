package servicos;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dominio.NotaAvaliacao;
import negocio.NotaAvaliacaoService;

@Stateless
@Path("/consulta")
public class NotaAvaliacaoRest {
@Inject private NotaAvaliacaoService notaAvaliacaoService;
	
	public NotaAvaliacaoRest(){
		
	}
	
	@GET
	@Path("/notas/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public NotaAvaliacao getNotaAvaliacao(@PathParam("id") String id){
		return notaAvaliacaoService.getNotaAvaliacaoID(Long.parseLong(id));
	}
	
	@GET
	@Path("/notas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<NotaAvaliacao> listar(){
		return notaAvaliacaoService.listarNotaAvaliacao();
	}
	
	@DELETE
	@Path("/notas/{id}")
	public Response deleteMatricula(@QueryParam("id") long id){
		NotaAvaliacao p =  notaAvaliacaoService.getNotaAvaliacaoID(id);
		if(p==null){
			throw new NotFoundException();
		}
		
		notaAvaliacaoService.removerNotaAvaliacao(id);
		return Response.noContent().build();
	}
}
