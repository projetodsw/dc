package servicos;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dominio.Disciplina;
import negocio.DisciplinaService;

@Stateless
@Path("/consulta")
public class DisciplinaRest {
@Inject private DisciplinaService disciplinaService;
	
	public DisciplinaRest(){
		
	}
	
	@GET
	@Path("/disciplinas/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Disciplina getDisciplina(@PathParam("id") String id){
		return disciplinaService.getDisciplinaID(Long.parseLong(id));
	}
	
	@GET
	@Path("/disciplinas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Disciplina> listar(){
		return disciplinaService.listarDisciplina();
	}
	
	@DELETE
	@Path("/disciplinas/{id}")
	public Response deleteDisciplina(@QueryParam("id") long id){
		Disciplina p =  disciplinaService.getDisciplinaID(id);
		if(p==null){
			throw new NotFoundException();
		}
		
		disciplinaService.removerDisciplina(id);
		return Response.noContent().build();
	}
}
