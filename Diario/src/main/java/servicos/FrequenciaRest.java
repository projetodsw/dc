package servicos;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dominio.Frequencia;
import negocio.FrequenciaService;

@Stateless
@Path("/consulta")
public class FrequenciaRest {
@Inject private FrequenciaService frequenciaService;
	
	public FrequenciaRest(){
		
	}
	
	@GET
	@Path("/frequencias/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Frequencia getFrequencia(@PathParam("id") String id){
		return frequenciaService.getFrequenciaID(Long.parseLong(id));
	}
	
	@GET
	@Path("/frequencias")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Frequencia> listar(){
		return frequenciaService.listarFrequencia();
	}
	
	@DELETE
	@Path("/frequencias/{id}")
	public Response deleteProfessor(@QueryParam("id") long id){
		Frequencia p =  frequenciaService.getFrequenciaID(id);
		if(p==null){
			throw new NotFoundException();
		}
		
		frequenciaService.removerFrequencia(id);
		return Response.noContent().build();
	}
}
