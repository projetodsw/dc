package servicos;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dominio.Aluno;
import negocio.AlunoService;

@Stateless
@Path("/consulta")
public class AlunoRest {
@Inject private AlunoService alunoService;
	
	public AlunoRest(){
		
	}
	
	@GET
	@Path("/alunos/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Aluno getAluno(@PathParam("id") String id){
		return alunoService.getAlunoID(Long.parseLong(id));
	}
	
	@GET
	@Path("/alunos")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Aluno> listar(){
		return alunoService.listarAluno();
	}
	
	@DELETE
	@Path("/alunos/{id}")
	public Response deleteAluno(@QueryParam("id") long id){
		Aluno p =  alunoService.getAlunoID(id);
		if(p==null){
			throw new NotFoundException();
		}
		
		alunoService.removerAluno(id);
		return Response.noContent().build();
	}
}
