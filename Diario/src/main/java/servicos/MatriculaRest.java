package servicos;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dominio.Matricula;
import negocio.MatriculaService;

@Stateless
@Path("/consulta")
public class MatriculaRest {
@Inject private MatriculaService matriculaService;
	
	public MatriculaRest(){
		
	}
	
	@GET
	@Path("/matriculas/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Matricula getMatricula(@PathParam("id") String id){
		return matriculaService.getMatriculaID(Long.parseLong(id));
	}
	
	@GET
	@Path("/matriculas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Matricula> listar(){
		return matriculaService.listarMatricula();
	}
	
	@DELETE
	@Path("/matriculas/{id}")
	public Response deleteMatricula(@QueryParam("id") long id){
		Matricula p =  matriculaService.getMatriculaID(id);
		if(p==null){
			throw new NotFoundException();
		}
		
		matriculaService.removerMatricula(id);
		return Response.noContent().build();
	}
}
