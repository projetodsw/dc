package servicos;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dominio.Aula;
import negocio.AulaService;

public class AulaRest {
@Inject private AulaService aulaService;
	
	public AulaRest(){
		
	}
	
	@GET
	@Path("/aulas/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Aula getAula(@PathParam("id") String id){
		return aulaService.getAulaID(Long.parseLong(id));
	}
	
	@GET
	@Path("/aulas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Aula> listar(){
		return aulaService.listarAula();
	}
	
	@DELETE
	@Path("/aulas/{id}")
	public Response deleteAula(@QueryParam("id") long id){
		Aula p =  aulaService.getAulaID(id);
		if(p==null){
			throw new NotFoundException();
		}
		
		aulaService.removerAula(id);
		return Response.noContent().build();
	}
}
