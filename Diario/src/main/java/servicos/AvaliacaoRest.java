package servicos;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dominio.Avaliacao;
import negocio.AvaliacaoService;

@Stateless
@Path("/consulta")
public class AvaliacaoRest {
@Inject private AvaliacaoService avaliacaoService;
	
	public AvaliacaoRest(){
		
	}
	
	@GET
	@Path("/avaliacoes/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Avaliacao getAvaliacao(@PathParam("id") String id){
		return avaliacaoService.getAvaliacaoID(Long.parseLong(id));
	}
	
	@GET
	@Path("/avaliacoes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Avaliacao> listar(){
		return avaliacaoService.listarAvaliacao();
	}
	
	@DELETE
	@Path("/avaliacoes/{id}")
	public Response deleteAvaliacao(@QueryParam("id") long id){
		Avaliacao p =  avaliacaoService.getAvaliacaoID(id);
		if(p==null){
			throw new NotFoundException();
		}
		
		avaliacaoService.removerAvaliacao(id);
		return Response.noContent().build();
	}
}
