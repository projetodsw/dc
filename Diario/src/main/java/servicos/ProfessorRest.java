package servicos;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dominio.Professor;
import negocio.ProfessorService;

@Stateless
@Path("/consulta")
public class ProfessorRest {
	@Inject private ProfessorService professorService;
	
	public ProfessorRest(){
		
	}
	
	@GET
	@Path("/professores/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Professor getProfessor(@PathParam("id") String id){
		return professorService.getProfessorID(Long.parseLong(id));
	}
	
	@GET
	@Path("/professores")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Professor> listar(){
		return professorService.listarProfessor();
	}
	
	@DELETE
	@Path("/professores/{id}")
	public Response deleteProfessor(@QueryParam("id") long id){
		Professor p =  professorService.getProfessorID(id);
		if(p==null){
			throw new NotFoundException();
		}
		
		professorService.removerProfessor(id);
		return Response.noContent().build();
	}
}
