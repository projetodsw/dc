package servicos;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dominio.Turma;
import negocio.TurmaService;

@Stateless
@Path("/consulta")
public class TurmaRest {
@Inject private TurmaService turmaService;
	
	public TurmaRest(){
		
	}
	
	@GET
	@Path("/turmas/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Turma getTurma(@PathParam("id") String id){
		return turmaService.getTurmaID(Long.parseLong(id));
	}
	
	@GET
	@Path("/turmas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Turma> listar(){
		return turmaService.listarTurma();
	}
	
	@DELETE
	@Path("/turmas/{id}")
	public Response deleteTurma(@QueryParam("id") long id){
		Turma p =  turmaService.getTurmaID(id);
		if(p==null){
			throw new NotFoundException();
		}
		
		turmaService.removerTurma(id);
		return Response.noContent().build();
	}
}
