package negocio;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.AulaDAO;
import dominio.Aula;

@Stateful
public class AulaService {
	@Inject
	private AulaDAO aulaDAO;
	
	public List<Aula> listarAula() {
		return aulaDAO.listar();
	}
	
	public Aula getAulaID(long id){
		return aulaDAO.buscarAulaID(id);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarAulaS(Aula aula) {
		Aula a = aulaDAO.buscar(aula.getTitulo());
		if (a == null) {
			aulaDAO.salvar(aula);
		} else {
			aulaDAO.atualizar(aula);
		}
	}
	
	public void removerAula(long id) {
		Aula p = aulaDAO.buscarAulaID(id);
		if(p!=null){
			aulaDAO.remover(p);
		}
	}
}
