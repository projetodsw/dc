package negocio;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.ProfessorDAO;
import dominio.Professor;

@Stateful
public class ProfessorService {
	@Inject
	private ProfessorDAO professorDAO;
	
	public List<Professor> listarProfessor() {
		return professorDAO.listar();
	}
	

	public Professor getProfessorID(long id){
		return professorDAO.buscarProfessorID(id);
	}

	public void removerProfessor(Professor c){
		professorDAO.remover(c);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarProfessor(Professor professor) {
		Professor p = professorDAO.buscar(professor.getNome());
		if (p == null) {
			professorDAO.salvar(professor);
		} else {
			professorDAO.atualizar(professor);
		}
	}

	public void removerProfessor(long id) {
		Professor p = professorDAO.buscarProfessorID(id);
		if(p!=null){
			professorDAO.remover(p);
		}
	}

}
