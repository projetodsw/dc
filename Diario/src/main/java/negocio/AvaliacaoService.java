package negocio;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.AvaliacaoDAO;
import dominio.Avaliacao;

@Stateful
public class AvaliacaoService {
	@Inject
	private AvaliacaoDAO avaliacaoDAO;
	
	public List<Avaliacao> listarAvaliacao() {
		return avaliacaoDAO.listar();
	}
	
	public Avaliacao getAvaliacaoID(long id){
		return avaliacaoDAO.buscarAvaliacaoID(id);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarAvaliacao(Avaliacao avaliacao) {
		Avaliacao a = avaliacaoDAO.buscar(avaliacao.getTitulo());
		if (a == null) {
			avaliacaoDAO.salvar(avaliacao);
		} else {
			avaliacaoDAO.atualizar(avaliacao);
		}
	}
	
	public void removerAvaliacao(long id) {
		Avaliacao p = avaliacaoDAO.buscarAvaliacaoID(id);
		if(p!=null){
			avaliacaoDAO.remover(p);
		}
	}
}
