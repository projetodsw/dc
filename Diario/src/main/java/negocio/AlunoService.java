package negocio;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.AlunoDAO;
import dominio.Aluno;

@Stateful
public class AlunoService {
	@Inject
	private AlunoDAO alunoDAO;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Aluno> listarAluno() {
		return alunoDAO.listar();
	}

	public Aluno getAlunoID(long id){
		return alunoDAO.buscarAlunoID(id);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarAluno(Aluno aluno) {
		Aluno a = alunoDAO.buscar(aluno.getNome());
		if (a == null) {
			alunoDAO.salvar(aluno);
		} else {
			alunoDAO.atualizar(aluno);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removerAluno(long id) {
		Aluno p = alunoDAO.buscarAlunoID(id);
		if(p!=null){
			alunoDAO.remover(p);
		}
	}
}
