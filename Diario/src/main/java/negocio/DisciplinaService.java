package negocio;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.DisciplinaDAO;
import dominio.Disciplina;



@Stateful
public class DisciplinaService {
	@Inject
	private DisciplinaDAO disciplinaDAO;
	
	public List<Disciplina> listarDisciplina() {
		return disciplinaDAO.listar();
	}
	
	public Disciplina getDisciplinaID(long id){
		return disciplinaDAO.buscarDisciplinaID(id);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarDisciplina(Disciplina disciplina) {
		Disciplina a = disciplinaDAO.buscar(disciplina.getNome());
		if (a == null) {
			disciplinaDAO.salvar(disciplina);
		} else {
			disciplinaDAO.atualizar(disciplina);
		}
	}
	
	public void removerDisciplina(long id) {
		Disciplina p = disciplinaDAO.buscarDisciplinaID(id);
		if(p!=null){
			disciplinaDAO.remover(p);
		}
	}
}
