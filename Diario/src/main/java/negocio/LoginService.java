package negocio;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.UsuarioDAO;
import dominio.Usuario;

@Stateless
public class LoginService {
	@Inject
	private UsuarioDAO usuarioDAO;

	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}

	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int login(String l, String s) {
		Usuario u = usuarioDAO.buscarLogin(l);
		if (u != null) {
			if (u.getSenha().equals(s)) {
				switch (u.getPermissao()){
				case 0: return 0; //login e senha corretos para admin
		
				case 1: return 1;//login e senha corretos para professor
				
				case 2: return 2; //login e senha corretos para aluno
				
				}
			} else return -1; // senha incorreta

		} return -2; // usuario inexistente

	}
	
}
