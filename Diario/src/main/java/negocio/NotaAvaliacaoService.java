package negocio;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.NotaAvaliacaoDAO;
import dominio.NotaAvaliacao;

@Stateful
public class NotaAvaliacaoService {
	@Inject
	private NotaAvaliacaoDAO notaAvaliacaoDAO;
	
	public List<NotaAvaliacao> listarNotaAvaliacao() {
		return notaAvaliacaoDAO.listar();
	}
	
	public NotaAvaliacao getNotaAvaliacaoID(long id){
		return notaAvaliacaoDAO.buscarNotaAvaliacaoID(id);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarNotaAvaliacao(NotaAvaliacao notaAvaliacao) {
		NotaAvaliacao a = notaAvaliacaoDAO.buscar(notaAvaliacao.getId());
		if (a == null) {
			notaAvaliacaoDAO.salvar(notaAvaliacao);
		} else {
			notaAvaliacaoDAO.atualizar(notaAvaliacao);
		}
	}
	
	public void removerNotaAvaliacao(long id) {
		NotaAvaliacao p = notaAvaliacaoDAO.buscarNotaAvaliacaoID(id);
		if(p!=null){
			notaAvaliacaoDAO.remover(p);
		}
	}
}
