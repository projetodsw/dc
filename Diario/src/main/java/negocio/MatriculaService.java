package negocio;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.MatriculaDAO;
import dominio.Matricula;

@Stateful
public class MatriculaService {
	@Inject
	private MatriculaDAO matriculaDAO;
	
	public List<Matricula> listarMatricula() {
		return matriculaDAO.listar();
	}
	
	public Matricula getMatriculaID(long id){
		return matriculaDAO.buscarMatriculaID(id);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarMatricula(Matricula  matricula) {
		Matricula a = matriculaDAO.buscar( matricula.getMatricula());
		if (a == null) {
			matriculaDAO.salvar( matricula);
		} else {
			matriculaDAO.atualizar( matricula);
		}
	}
	
	public void removerMatricula(long id) {
		Matricula p = matriculaDAO.buscarMatriculaID(id);
		if(p!=null){
			matriculaDAO.remover(p);
		}
	}
}
