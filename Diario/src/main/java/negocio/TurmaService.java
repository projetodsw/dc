package negocio;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.TurmaDAO;
import dominio.Turma;

@Stateful
public class TurmaService {
	@Inject
	private TurmaDAO turmaDAO;
	
	public List<Turma> listarTurma() {
		return turmaDAO.listar();
	}
	
	public Turma getTurmaID(long id){
		return turmaDAO.buscarTurmaID(id);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarTurma(Turma  turma) {
		Turma a = turmaDAO.buscar(turma.getNome());
		if (a == null) {
			turmaDAO.salvar( turma);
		} else {
			turmaDAO.atualizar( turma);
		}
	}
	
	public void removerTurma(long id) {
		Turma p = turmaDAO.buscarTurmaID(id);
		if(p!=null){
			turmaDAO.remover(p);
		}
	}
}
