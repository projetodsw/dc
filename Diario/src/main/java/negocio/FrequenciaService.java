package negocio;

import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import dao.FrequenciaDAO;
import dominio.Frequencia;

@Stateful
public class FrequenciaService {
	@Inject
	private FrequenciaDAO frequenciaDAO;
	
	public List<Frequencia> listarFrequencia() {
		return frequenciaDAO.listar();
	}
	
	public Frequencia getFrequenciaID(long id){
		return frequenciaDAO.buscarFrequenciaID(id);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cadastrarFrequencia(Frequencia frequencia) {
		Frequencia a = frequenciaDAO.buscar(frequencia.getId());
		if (a == null) {
			frequenciaDAO.salvar(frequencia);
		} else {
			frequenciaDAO.atualizar(frequencia);
		}
	}
	
	public void removerFrequencia(long id) {
		Frequencia p = frequenciaDAO.buscarFrequenciaID(id);
		if(p!=null){
			frequenciaDAO.remover(p);
		}
	}
}
